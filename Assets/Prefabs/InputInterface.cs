﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputInterface : MonoBehaviour {
    float snapThreshold = 0.08f;
    Dictionary<string, int> partEnum = new Dictionary<string, int>()
    {
        { "JOINT", 1},
        { "SHORT", 2},
        { "PLANK", 3},
        { "CAP", 4},
        { "LINER", 5},
        { "JOINT_HOLO", 1},
        { "SHORT_HOLO", 2},
        { "PLANK_HOLO", 3},
        { "CAP_HOLO", 4},
        { "LINER_HOLO", 5}
    };


    public GameObject getBestHoloMatch(Transform actualObject)
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("HoloZone");


        List<float> distances = new List<float>();
        for (int i = 0; i < objs.Length; i++)
        {
            int holoType;
            int actualType;
            try
            {
                holoType = partEnum[objs[i].name.ToUpper()];
                actualType = partEnum[actualObject.name.ToUpper()];
            }
            catch (System.Exception e)
            {
                return null;
            }
            

            if (holoType == actualType)
            {
                HoloTriggerMatch hitbox = objs[i].GetComponent<HoloTriggerMatch>();
                float dist = hitbox.matchConfidence(actualObject);
                distances.Add(dist);
            }
            else
            {
                distances.Add(10000f);
            }

            


        }

        int minIndex = 0;
        float minValue = 10000f;
        for (int i = 0; i < distances.Count; i++)
        {
            if (distances[i] < minValue)
            {
                minValue = distances[i];
                minIndex = i;
            }
        }

        if (minValue < snapThreshold)
        {
            print("Distance: " + minValue.ToString());
            return objs[minIndex];            
        }

        return null;
        
        
    }
}
