﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoloTriggerMatch : MonoBehaviour {
    public float matchConfidence(Transform actualTransform)
    {
        //Debug.Log(actualTransform.position);
        Transform holoTransform = transform;
        //Debug.Log(holoTransform.position);
        float positionDist = Vector3.Distance(actualTransform.position, holoTransform.position);
        //Debug.Log(positionDist);

        return positionDist;
    }
}
