﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectCollider : MonoBehaviour {
    public string hand = "";
    static int score = 0;
    public Text text;
    public GameObject deathEffect;

    public bool destroyOncomingPoseBlocks = true;

    void Start()
    {
        text = GameObject.Find("ScoreText").GetComponent<Text>();
    }

    public void ActivateCollision()
    {
        destroyOncomingPoseBlocks = true;
    }

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.name);
        try
        {
            if(destroyOncomingPoseBlocks)
            {
                string handID = other.gameObject.GetComponent<PlayerPose>().hand;
                //Debug.Log(handID + "==" + this.hand);
                if (handID == this.hand)
                {
                    //Debug.Log(hand + " has scored 1 point!");
                    score += 1;
                    text.text = "Score: " + score.ToString();
                    if (deathEffect != null)
                        Instantiate(deathEffect, transform.position, Quaternion.identity);
                    GameObject.Destroy(this.gameObject);
                }
            }

        }
        catch
        {
            
        }

        if(other.name == "Walls")
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
