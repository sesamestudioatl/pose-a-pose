﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteIfNoChildren : MonoBehaviour {
    int childCount;

	// Use this for initialization
	void Start () {
        
        

	}
	
	// Update is called once per frame
	void Update () {
        childCount = 0;
        foreach (Transform child in transform)
        {
            childCount += 1;
            //child is your child transform
        }
        if (childCount == 0 )
        {
            GameObject.Destroy(this.gameObject);
        }
	}
}
