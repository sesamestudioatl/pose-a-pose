﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoseSpawn : MonoBehaviour {
    public GameObject[] posePrefabs;
    public float spawnInterval = 0f;
    float initialSpawnTime;
    float nextSpawnTime;



    void Start()
    {
        initialSpawnTime = 2f;
        nextSpawnTime = initialSpawnTime;
    }

    void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            //Debug.Log("Spawn Object");
            //0<=x<2
            int randInt = Random.RandomRange(0, posePrefabs.Length);
            GameObject obj = (GameObject)Instantiate(posePrefabs[randInt]);
            
            float orig_scale = obj.GetComponent<Scaler>().wingSpan;
            float new_scale = EnvSettings.scalarDistanceStandard;
            if (new_scale == 0)
                new_scale = orig_scale;
                
            float scaleAmount = new_scale/orig_scale;
            //Debug.Log(new_scale);
            Vector3 newScale = new Vector3(0f, 0f, 0f);
            newScale.x = obj.transform.localScale.x * scaleAmount;
            newScale.y = obj.transform.localScale.y * scaleAmount;
            newScale.z = obj.transform.localScale.z * scaleAmount;
            obj.transform.localScale = newScale;

            //Change scale for all children
            foreach (Transform child in obj.transform)
            {
                GameObject child_temp = child.gameObject;
                float childScaleAmount = orig_scale / new_scale;
                //Debug.Log(new_scale);
                Vector3 newChildScale = new Vector3(0f, 0f, 0f);
                newChildScale.x = child_temp.transform.localScale.x * childScaleAmount;
                newChildScale.y = child_temp.transform.localScale.y * childScaleAmount;
                newChildScale.z = child_temp.transform.localScale.z * childScaleAmount;
                child_temp.transform.localScale = newChildScale;
            }

            obj.transform.parent = this.transform;
            
            nextSpawnTime = Time.time + spawnInterval;
            
        }
        

    }

    public Transform leftHand;
    public Transform rightHand;
    public Transform head;
    public void updateScaler()
    {
        float distance = Vector3.Distance(leftHand.position, rightHand.position);
        Debug.Log(distance);
        GameObject.Find("LCP").GetComponent<EnvSettings>().UpdateScaler(distance);
    }
}
