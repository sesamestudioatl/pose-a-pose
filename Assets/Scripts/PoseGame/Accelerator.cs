﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerator : MonoBehaviour {
    public Vector3 speedVector;
    float speedMagnitude = 0.02f;

    void Start()
    {
        if (EnvSettings.lcp == "QA")
        {
            speedMagnitude = 0f;
        }
        else if (EnvSettings.lcp == "INSTRUCTOR")
        {
            speedMagnitude = speedMagnitude;
        }
        else if (EnvSettings.lcp == "PR")
        {
            speedMagnitude = speedMagnitude;
        }
    }

    void Update()
    {
        Vector3 target = speedVector + transform.position;
        Vector3 newPos = Vector3.MoveTowards(transform.position, target, speedMagnitude);
        transform.position = newPos;
    }
}
