﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvSettings : MonoBehaviour {
    public string lcp_setting = "QA";
    public static string lcp;
    public float scalarDistance = 0f;
    public static float scalarDistanceStandard = 0f;

    void Start()
    {
        lcp = lcp_setting;
    }

    public void UpdateScaler(float dist)
    {
        scalarDistanceStandard = dist;
        scalarDistance = dist;
    }
}
