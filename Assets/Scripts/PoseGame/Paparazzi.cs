﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paparazzi : MonoBehaviour {
    public Transform leftHand;
    public Transform rightHand;
    public Transform head;

    public GameObject leftHandPoser;
    public GameObject rightHandPoser;
    public GameObject headPoser;

    public Transform parent;

    GameObject leftObj = null;
    GameObject rightObj = null;
    GameObject headObj = null;

    float actorWingSpan = 0f;

    bool canSnap = true;
    public Vector3 offset = new Vector3(0f, 0f, 4f);

    public void UnityChanSays()
    {
        SnapPhoto("Controller (right)");
    }

    float spawnIncrements = 1.5f;
    float nextSpawnTime = 2f;
    void Update()
    {
        if (EnvSettings.lcp == "INSTRUCTOR")
        {
            if (Time.time > nextSpawnTime)
            {
                UnityChanSays();
                nextSpawnTime += spawnIncrements;
            }
        }
    }

    public void SnapPhoto(string controllerName)
    {
        //Debug.Log(controllerName + "==" + "Controller (right)");
        if(controllerName == "Controller (right)" && canSnap)
        {
            if (EnvSettings.lcp != "INSTRUCTOR")
                canSnap = false;


            leftObj = (GameObject)Instantiate(leftHandPoser, leftHand.position + offset, leftHand.rotation);
            rightObj = (GameObject)Instantiate(rightHandPoser, rightHand.position + offset, rightHand.rotation);
            headObj = (GameObject)Instantiate(headPoser, head.position + offset, head.rotation);

            leftObj.GetComponent<Accelerator>().speedVector = new Vector3 (0f, 0f, -1f);
            rightObj.GetComponent<Accelerator>().speedVector = new Vector3(0f, 0f, -1f);
            headObj.GetComponent<Accelerator>().speedVector = new Vector3(0f, 0f, -1f);

            leftObj.GetComponent<DetectCollider>().ActivateCollision();
            rightObj.GetComponent<DetectCollider>().ActivateCollision();
            headObj.GetComponent<DetectCollider>().ActivateCollision();

        
            
            leftObj.transform.parent = parent;
            rightObj.transform.parent = parent;
            headObj.transform.parent = parent;
        

        }
        else if (controllerName == "Controller (left)")
        {
            canSnap = true;
            GameObject.Destroy(leftObj);
            leftObj = null;
            GameObject.Destroy(rightObj);
            rightObj = null;
            GameObject.Destroy(headObj);
            headObj = null;
        }
        
    }

    public void updateScaler()
    {
        EnvSettings.scalarDistanceStandard = Vector3.Distance(leftHand.position, rightHand.position);
        parent.gameObject.GetComponent<Scaler>().setWingSpan(EnvSettings.scalarDistanceStandard);
    }
}
