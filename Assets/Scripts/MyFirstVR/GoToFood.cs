﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToFood : MonoBehaviour {
    public bool harvestClosestFood = false;
    public float speed = 0.05f;

    void Update () 
    {
		if (harvestClosestFood)
        {
            Transform target = GetClosestGameObjectLocation("Food");
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed);
        }
	}

    Transform GetClosestGameObjectLocation(string tag)
    {
        GameObject[] objectList = GameObject.FindGameObjectsWithTag("Food");

        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject obj in objectList)
        {
            Transform potentialTarget = obj.transform;
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
}
