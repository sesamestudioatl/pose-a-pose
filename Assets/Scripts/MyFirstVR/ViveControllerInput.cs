﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveControllerInput : MonoBehaviour 
{
    private SteamVR_TrackedObject trackedObj;
    
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    void Update()
    {
        if (Controller.GetAxis() != Vector2.zero)
        {
            //Debug.Log(gameObject.name + Controller.GetAxis());
        }

        if (Controller.GetHairTriggerDown())
        {
            if ((EnvSettings.lcp == "QA") || (EnvSettings.lcp == "INSTRUCTOR"))
            {
                GameObject.Find("Paparazzi").GetComponent<Paparazzi>().SnapPhoto(gameObject.name);
            }
            
            
            //Debug.Log(gameObject.name + " Trigger Press");
        }

        if (Controller.GetHairTriggerUp())
        {
            //Debug.Log(gameObject.name + " Trigger Release");
        }

        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            if (EnvSettings.lcp == "PR")
                GameObject.Find("PoseSpawner").GetComponent<PoseSpawn>().updateScaler();
            else if (EnvSettings.lcp == "QA")
                GameObject.Find("Paparazzi").GetComponent<Paparazzi>().updateScaler();
            //Debug.Log(gameObject.name + " Grip Press");
        }

        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            //Debug.Log(gameObject.name + " Grip Release");
        }
    }
}
