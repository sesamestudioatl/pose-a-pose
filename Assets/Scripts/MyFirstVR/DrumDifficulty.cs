﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumDifficulty : MonoBehaviour {

    public int level = 1;
    public float levelTime = 10f;
    public GameObject[] drums;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time > levelTime)
        {
            level += 1;
            levelTime = Time.time + 10f;
            IncreaseDifficulty();
        }

	
	}

    private void IncreaseDifficulty()
    {
        for (int i = 0; i < drums.Length; i++) {
            drums[i].GetComponent<ColorChanger>().IncreaseDifficulty(level);

        }
    }
}
