﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespondToWhacks : MonoBehaviour {
    public RWVR_InteractionController leftController, rightController;

    void OnCollisionEnter(Collision collision)
    {
       
        if (collision.relativeVelocity.magnitude > 2)
        {
            Debug.Log("Woosh");
        }

        if (collisionCausedByHeldObject(collision))
        {
            Debug.Log("The " + collision.gameObject.name + " hit me!");
        }
        else
        {
            //Debug.Log("Something else hit me: " + collision.gameObject.name);
        }
    }

    private bool collisionCausedByHeldObject(Collision collision)
    {
        if (leftController.InteractionObject)
        {
            if (collision.gameObject == leftController.InteractionObject.gameObject)
            {
                //Debug.Log("Hit by left controller: " + collision.gameObject.name);
                return true;
            }
        }

        if (rightController.InteractionObject)
        {
            if (collision.gameObject == rightController.InteractionObject.gameObject)
            {
                //Debug.Log("Hit by right controller: " + collision.gameObject.name);
                return true;
            }
        }
        return false;
    }
}
