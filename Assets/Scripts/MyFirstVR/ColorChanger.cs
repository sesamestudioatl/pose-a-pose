﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour {
    public Material origMaterial;
    public Material hitMaterial;

    public bool punchable = false;
    public float randomTime;

    public float minTime, maxTime;

    Renderer rend;
    private SoundController soundEffect;
    
    void Start()
    {
        rend = GetComponent<Renderer>();
        origMaterial = rend.material;

        soundEffect = GetComponent<SoundController>();

        startFuse();
    }

    void Update()
    {
        if (isFuseBlown())
        {
            PunchMe(); 
        }

        UpdateColor();
            
    }

    private bool isFuseBlown()
    {
        return Time.time > randomTime;
    }

    private void startFuse()
    {
        randomTime = Random.Range(minTime, maxTime) + Time.time;
    }

    private void PunchMe()
    {
        if (!punchable)
        {
            soundEffect.Play();
        }
        punchable = true;

    }

    private void UpdateColor()
    {
        if (punchable)
        {
            rend.material = hitMaterial;
        }
        else
        {
            rend.material = origMaterial;
        }
    }

    internal void Punched()
    {
        punchable = false;
        startFuse();
    }

    internal void IncreaseDifficulty(int level)
    {
        maxTime -= level;
        if (maxTime < minTime) {
            maxTime = minTime;
        }
       
    }
}
