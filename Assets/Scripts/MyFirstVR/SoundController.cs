﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {
    public AudioSource activateSE;

    void Start()
    {
        activateSE = GetComponent<AudioSource>();
    }

    public void Play()
    {
        activateSE.Play();
    }
}
