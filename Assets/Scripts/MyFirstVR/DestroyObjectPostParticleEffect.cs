﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectPostParticleEffect : MonoBehaviour {
    ParticleSystem partSystem;
    float lifetime = 0f;
    float spawnTime = 0f;

	void Start () {
        partSystem = GetComponent<ParticleSystem>();
        lifetime = partSystem.main.startLifetime.constant;
        spawnTime = Time.time;
	}

    void Update()
    {
        if (Time.time - spawnTime > lifetime)
        {
            GameObject.Destroy(this.gameObject);
        }
    }
	
}
