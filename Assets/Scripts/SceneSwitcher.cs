﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {
    bool startingGame = false;
    public string sceneName = "LightSwitch";

    void OnTriggerEnter(Collider other)
    {
        if (!startingGame)
        {
            StartCoroutine(LoadYourAsyncScene());
            startingGame = true;
        }
        
    }

    IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
