﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BLVR_SqueezeSpawn : MonoBehaviour {
    private SteamVR_TrackedObject trackedObj;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    public GameObject spawnObjectPrefab;
    void Update()
    {
        RWVR_InteractionController controller = GetComponent<RWVR_InteractionController>();
        if (!controller.InteractionObject)
        {
            if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
                //Debug.Log(gameObject.name + " Grip Press");
                GameObject obj = (GameObject)Instantiate(spawnObjectPrefab, transform.position, transform.rotation);
                if (Controller.GetHairTrigger())
                {
                    //if trigger is being held
                    controller.autoAttachObject();
                }
                
            }

            if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
            {
                //Debug.Log(gameObject.name + " Grip Release");
            }
        }
        
    }

}
